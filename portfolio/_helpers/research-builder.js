const Cite = require('citation-js');
const fs = require("fs");
const path = require("node:path")

const dois = [
    "10.1109/ECCE47101.2021.9595911",
    "10.1049/icp.2022.1642",
    "10.1049/icp.2022.1680",
    "10.36688/ewtec-2023-268",
    "10.1109/IEMDC55163.2023.10238864",
    "10.1049/icp.2022.1009"
]

let citations = [];

for (let i=0; i < dois.length; i++) {
    citations.push(new Cite(dois[i]));
    citations[i].citation = doiConvert(citations[i]);
}

citations.sort(function(a,b) {
    return a.data[0].created["timestamp"] - b.data[0].created["timestamp"]
});

const data = JSON.stringify(citations, null, 2);
const outputPath = path.join(__dirname, "..", "_data","research.json");

fs.writeFile(outputPath, data, (error) => {
    if (error) {
        console.error(error)
        throw error;
    }

    console.log("\"%s\" written correctly", outputPath);
});


function doiConvert(citation) {
    let formatted = citation.format('bibliography', {
        format: 'text',
        template: 'apa',
        lang: 'en-GB'
    });

    return formatted;
}