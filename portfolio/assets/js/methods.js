function navDropdownClicked() {
    $("#nav-right a:not(:last-child)").toggleClass("visible");
}

function onResize() {
    $("#wrapper").css({
        "margin-top": $("#topnav").height()
    });
}

// Contact form
function submitForm() {

    if (validateForm()) {
        var form = document.getElementsByName('contact-form')[0];
        form.submit();
        form.reset();
    }
}
function validateForm() {

    var form =  document.getElementById("contact-form");
    var warnings = form.getElementsByClassName("validation-warning");
    var fields = form.getElementsByClassName("contact-fields__field");
    var valid = true;

    for (let i=0; i < fields.length-1; i++) {
        let field = fields[i].lastElementChild;
        let warning = warnings[i];
        let name = field.name

        if (emptyOrWhitespace(field.value)) {
            warning.textContent = "* Required Field"
            warning.style.color = "#FF4533";
            valid = false;
        } else if (name == 'email') {
            if (!validateEmail(field.value)) {
                warning.textContent = "* Invalid Email"
                warning.style.color = "#FF4533"
                valid = false;
            } else {
                warning.style.color = 'transparent'
            }
        } else {
            warning.style.color = "transparent";
        }
    }
    return valid;
}

function resetForm() {
    var list = document.getElementById("four").getElementsByClassName("row gtr-uniform")[0].children;
    for (let i=0; i < list.length-1; i++) {
        let warning = list[i].firstElementChild;
        warning.style.color = "transparent";
    }
}

function validateEmail(str) {
    regex = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$"
    match = str.match(regex)

    if (match == null) {
        return false
    } else {
        return true
    }
}

function emptyOrWhitespace(str) {
    return str == null || str.length == 0 || str.match('^\s+$') != null
}
